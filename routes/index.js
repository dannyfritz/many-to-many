var express = require('express');
var router = express.Router();
var db = require("../db/knex")
var _ = require("lodash")

/* GET home page. */
router.get('/', function(req, res, next) {
  db('author_book')
    .select('book.name as book', 'author.name as author')
    .join('book', 'book_id', 'book.id')
    .join('author', 'author_id', 'author.id')
    .then (function (books) {
      books = _.map(_.groupBy(books, "book"), function (authors, book) {
        return {
          name: book,
          authors: _.map(authors, function (data) {
            return {
              name: data.author
            }
          })
        }
      })
      console.log(books)
      res.render('index', {
        title: 'Reads',
        books: books
      });
    })
    .catch(function (reason) {
      next(reason)
    })
});

module.exports = router;
