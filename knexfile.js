require("dotenv").config()

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: "./db.sqlite"
    }
  },

  production: {
    client: 'pg',
    connection: process.env.DATABASE_URL
  }

};
