
exports.seed = function(knex, Promise) {
  return knex('book').del()
    .then(function () {
      return Promise.all([
        knex('book').insert({id: 1, name: 'You don\'t know JS'}),
        knex('book').insert({id: 2, name: 'JS the Good Parts'})
      ]);
    });
};
