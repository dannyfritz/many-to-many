
exports.seed = function(knex, Promise) {
  return knex('author').del()
    .then(function () {
      return Promise.all([
        knex('author').insert({id: 1, name: 'Kyle Simpson'}),
        knex('author').insert({id: 2, name: 'Douglas Crockford'})
      ]);
    });
};
