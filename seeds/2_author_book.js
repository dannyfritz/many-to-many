

exports.seed = function(knex, Promise) {
  return knex('author_book').del()
    .then(function () {
      return Promise.all([
        knex('author_book').insert({book_id: '1', author_id: '1'}),
        knex('author_book').insert({book_id: '1', author_id: '2'}),
        knex('author_book').insert({book_id: '2', author_id: '2'}),
      ]);
    });
};
